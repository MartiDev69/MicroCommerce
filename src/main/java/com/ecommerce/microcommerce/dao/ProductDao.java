package com.ecommerce.microcommerce.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.microcommerce.model.Product;

@Repository
public interface ProductDao extends CrudRepository<Product, Integer> {
	Product findById(int id);

	List<Product> findByPrixGreaterThan(int prixLimit);

	Iterable<Product> findAll(Sort sort);

}