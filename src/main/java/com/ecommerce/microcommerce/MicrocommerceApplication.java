package com.ecommerce.microcommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MicrocommerceApplication {

	public static void main(String[] args) {
		// https://openclassrooms.com/fr/courses/4668056-construisez-des-microservices/7652030-testez-votre-api-grace-a-postman
		SpringApplication.run(MicrocommerceApplication.class, args);
	}

}
