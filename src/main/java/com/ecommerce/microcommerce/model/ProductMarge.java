package com.ecommerce.microcommerce.model;

public class ProductMarge {
	public ProductMarge(Product produit, int marge) {
		super();
		this.produit = produit;
		this.marge = marge;
	}

	Product produit;
	int marge;

	public Product getProduit() {
		return produit;
	}

	public void setProduit(Product produit) {
		this.produit = produit;
	}

	public int getMarge() {
		return marge;
	}

	public void setMarge(int marge) {
		this.marge = marge;
	}
}
